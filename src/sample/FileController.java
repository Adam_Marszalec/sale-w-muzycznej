package sample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileController {
    public static List<Student> getStudentsFromFile(){
        List<Student> students = new ArrayList<>(6);
        File file = new File("students.txt");
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String[] line = scanner.nextLine().split(";");
                String name = line[0];
                int year = Integer.parseInt(line[1]);
                Student student = new Student (name, year);
                students.add(student);
            }
            scanner.close();
        } catch (FileNotFoundException Brutus) {
            Brutus.printStackTrace();
        }

        return students;
    }

    public static List<Room> getRoomsFromFile(){
        List<Room> rooms = new ArrayList<>(8);
        File file = new File("rooms.txt");
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String[] line = scanner.nextLine().split(";");
                int number = Integer.parseInt(line[0]);
                boolean piano = Boolean.parseBoolean(line[1]);
                Room room = new Room (number, piano);
                rooms.add(room);
            }
            scanner.close();
        } catch (FileNotFoundException Brutus) {
            Brutus.printStackTrace();
        }

        return rooms;
    }

    public static List<Booking> getBookingsFromFile(List<Room> rooms, List<Student> students){
        List<Booking> bookings = new ArrayList<>();
        Student student = null;
        LocalDate localDate = null;
        Room room = null;
        File file = new File("booking.txt");
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()) {
                String[] line = scanner.nextLine().split(";");
                localDate = LocalDate.parse(line[0]);
                int period = Integer.parseInt(line[1]);
                for (int i = 0; i < getRoomsFromFile().size(); i++) {
                    if (rooms.get(i).getNumber() == Integer.parseInt(line[2])) {
                        room = rooms.get(i);
                    }
                }
                for (int i = 0; i < getStudentsFromFile().size(); i++) {
                    if (students.get(i).getName() == line[3]) {
                        student = students.get(i);
                    }
                }
                Booking booking = new Booking(localDate, period, room, student);
                bookings.add(booking);
            }
            scanner.close();
        } catch (FileNotFoundException Brutus) {
            Brutus.printStackTrace();
        }

        return bookings;
    }

    public static void writeBookingToFile(List<Booking> bookings){
        try {
            File file = new File("booking.txt");
            PrintWriter printWriter = new PrintWriter(new FileOutputStream("booking.txt", true));
            for (int i = 0; i < bookings.size(); i++) {
            String data = bookings.get(i).getLocalDate() + ";" + bookings.get(i).getPeriod() + ";"
                        + bookings.get(i).getRoom().getNumber() + ";" + bookings.get(i).getStudent().getName();
                printWriter.println(data);
            }
            printWriter.close();
        } catch (FileNotFoundException Brutus) {
            Brutus.printStackTrace();
        }

    }






}