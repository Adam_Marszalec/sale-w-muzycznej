package sample;

import java.io.Serializable;
import java.time.LocalDate;

public class Booking {

    private LocalDate localDate;
    private int period;
    private Room room;
    private Student student;

    public Booking(LocalDate localDate, int period, Room room, Student student) {
        this.localDate = localDate;
        this.period = period;
        this.room = room;
        this.student = student;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;

    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    @Override
    public String toString() {
        return student.getName() + "\n" + String.valueOf(student.getYear());
    }
}
