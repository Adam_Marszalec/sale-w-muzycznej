package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Data {

    public List<Student> students = new ArrayList<>();
    public List<Booking> bookings = new ArrayList<>();
    public List<Room> rooms = new ArrayList<>();

    public Data(List<Student> students, List<Booking> bookings, List<Room> rooms){
        this.students = students;
        this.bookings = bookings;
        this.rooms = rooms;
    }


    public static void saveInFile(Data data) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("data.ser"))) {
            oos.writeObject(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Data readFromFile() {
        Data data = new Data(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("data.ser"))) {
            data = (Data) ois.readObject();
        } catch (FileNotFoundException | ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }




}
