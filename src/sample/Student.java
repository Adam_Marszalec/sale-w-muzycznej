package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Student {

    private String name;
    private int year;

    public Student(String name, int year) {
        this.name = name;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public static List getStudents(){

        List<Student> studentList = new ArrayList<>(12);
        Student s1 = new Student("Mateusz Jocz", 1);
        Student s2 = new Student("Adam Marszalec", 2);
        Student s3 = new Student("Filip Kozak", 3);
        Student s4 = new Student("Wlodzimierz Lenin", 4);
        Student s5 = new Student("Austriacki Akwarelista", 5);
        Student s6 = new Student("Krzysztof Kononowicz", 6);
        Student s7 = new Student("Otto Skorzenny", 7);
        Student s8 = new Student("Iosif Stalin", 8);
        Student s9 = new Student("Josip Tito", 9);
        Student s10 = new Student("Juliusz Cezar", 10);
        Student s11 = new Student("Oktawian August", 11);
        Student s12 = new Student("Leonidas Spartanin", 7);

        studentList.add(s1);
        studentList.add(s2);
        studentList.add(s3);
        studentList.add(s4);
        studentList.add(s5);
        studentList.add(s6);
        studentList.add(s7);
        studentList.add(s8);
        studentList.add(s9);
        studentList.add(s10);
        studentList.add(s11);
        studentList.add(s12);

        saveInFile(studentList);

        return studentList;
    }



    public static void saveInFile (List list){
        try (ObjectOutputStream oos = new ObjectOutputStream
                (new FileOutputStream("students.ser"))) {
            oos.writeObject(getStudents());
        } catch (IOException brutus) {
            brutus.printStackTrace();
        }
    }


    public static List readFromFile(){
        List list = new ArrayList(12); //musi byc zainicjowane
        try (ObjectInputStream ois = new ObjectInputStream
                (new FileInputStream("students.ser"))) {
            list = (List) ois.readObject();
        } catch (InvalidClassException brutus) {
            brutus.printStackTrace();
        } catch (IOException | ClassNotFoundException brutus) {
            brutus.printStackTrace();
        }
        return list;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='"  + '\'' +
                ", year=" + year +
                '}';
    }
}

