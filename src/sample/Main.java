package sample;


import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


import java.io.File;
import java.io.Serializable;
import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Main extends Application {

    Stage stage;
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
    LocalDate localDate = LocalDate.now();
    Text successText = new Text();
    Button submitButton = new Button("Book");
    Button backButton = new Button("Back");
    Data data = new Data(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    List<Booking> bookings = new ArrayList<>();
    private final TableView table = new TableView();

    @Override
    public void start(Stage primaryStage) throws Exception {
        stage = primaryStage;
        stage.setTitle("Booking Meister 1.0");
        menuLayout();
    }

    private void menuLayout() {
        VBox root = new VBox();
        root.setAlignment(Pos.CENTER);
        root.setSpacing(15);

        Text menuText = new Text(); //to bold
        menuText.setText("Room Booker 1.0");
        menuText.setFont(Font.font("Verdana", FontWeight.EXTRA_BOLD, 30));
        menuText.setFill(Color.RED);

        Button bookingButton = new Button("Booking");
        Button statButton = new Button("Statistics");
        Button calendarButton = new Button("Calendar");
        Button exitButton = new Button("Exit");

        bookingButton.setMinWidth(100);
        statButton.setMinWidth(100);
        calendarButton.setMinWidth(100);
        exitButton.setMinWidth(100);

        bookingButton.setOnAction(event -> bookingLayout());
        statButton.setOnAction(event -> statLayout());
        calendarButton.setOnAction(event -> calendarLayout());
        exitButton.setOnAction(event -> stage.close());


        root.getChildren().addAll(menuText, bookingButton, statButton, calendarButton, exitButton);

        stage.setScene(new Scene(root, 300, 300));
        stage.show();

    }

    private void bookingLayout() {

        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        root.setHgap(5);
        root.setVgap(15);


        this.successText = new Text();
        Label classText = new Label("Period:");
        Label nameText = new Label("Name:");
        Label roomText = new Label("Room:");
        Label pianoText = new Label("Piano:");
        Label dateText = new Label("Date:");

        ObservableList<String> roomNames = FXCollections.observableArrayList();
        ObservableList<String> classNames = FXCollections.observableArrayList("1", "2", "3", "4", "5", "6", "7");
        ObservableList<String> nameNames = FXCollections.observableArrayList();


        ComboBox classBox = new ComboBox(classNames);
        ComboBox nameBox = new ComboBox(nameNames);
        ComboBox roomBox = new ComboBox(roomNames);
        classBox.setMinWidth(170);
        nameBox.setMinWidth(170);
        roomBox.setMinWidth(170);

        CheckBox checkbox = new CheckBox();
        boolean isSelected = checkbox.isSelected();


        for (int i = 0; i < FileController.getStudentsFromFile().size(); i++) {
            nameNames.add(FileController.getStudentsFromFile().get(i).getName());
        }

        for (int i = 0; i < FileController.getStudentsFromFile().size(); i++) {
            roomNames.add(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()));
        }

        EventHandler<ActionEvent> eventos = new EventHandler<ActionEvent>() {

            public void handle(ActionEvent e) {

                if (checkbox.isSelected()) {
                    roomNames.clear();
                    for (int i = 0; i < FileController.getRoomsFromFile().size(); i++) {
                        if (FileController.getRoomsFromFile().get(i).isPiano()) {
                            roomNames.add(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()));
                        }
                    }
                } else {
                    roomNames.clear();
                    for (int i = 0; i < FileController.getRoomsFromFile().size(); i++) {
                        roomNames.add(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()));
                    }
                }
            }
        };






                DatePicker datePicker = new DatePicker(localDate);

                checkbox.setOnAction(eventos);
                Button backButton = new Button("Back");
                submitButton.setMinWidth(100);
                backButton.setMinWidth(100);
                root.setHalignment(backButton, HPos.RIGHT);

        Alert warning = new Alert(Alert.AlertType.ERROR);
        warning.setTitle("Error!");
        warning.setHeaderText(null);
        warning.setContentText("Please fill all the spots!");


        submitButton.setOnAction(event -> {
                    Booking booking = new Booking(null, 0, null, null);

                    if (datePicker.getValue() != null) {
                        booking.setLocalDate(datePicker.getValue());
                    }

                    if (classBox.getValue() != null) {
                        booking.setPeriod(Integer.parseInt(classBox.getValue().toString()));
                    }

                    if (nameBox.getValue() != null) {
                        for (int i = 0; i < FileController.getStudentsFromFile().size(); i++) {
                            if (nameBox.getValue().toString().equals(FileController.getStudentsFromFile().get(i).getName())) {
                                booking.setStudent(FileController.getStudentsFromFile().get(i));
                                break;
                            }
                        }
                    }

                    if (roomBox.getValue() != null) {
                        for (int i = 0; i < FileController.getRoomsFromFile().size(); i++) {
                            if (roomBox.getValue().equals(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()))) {
                                booking.setRoom(FileController.getRoomsFromFile().get(i));
                            }
                        }
                    }
                    if (roomBox.getValue() == null || nameBox.getValue() == null || datePicker.getValue() == null || classBox.getValue() == null){
                    warning.showAndWait();
                    }

                    if (roomBox.getValue() != null && nameBox.getValue() != null && datePicker.getValue() != null && classBox.getValue() != null){
                        bookings.add(booking);
                        FileController.writeBookingToFile(bookings);

                        Alert alert = new Alert(Alert.AlertType.INFORMATION);
                        alert.setTitle("Success!");
                        alert.setHeaderText(null);
                        alert.setContentText("Room booked successfully!");
                        alert.showAndWait();
                        bookings.clear();

                    }






                });

                backButton.setOnAction(event -> menuLayout());

                root.add(classText, 0, 0);
                root.add(nameText, 0, 1);
                root.add(pianoText, 0, 2);
                root.add(roomText, 0, 3);
                root.add(dateText, 0, 4);
                root.add(classBox, 1, 0);
                root.add(nameBox, 1, 1);
                root.add(checkbox, 1, 2);
                root.add(roomBox, 1, 3);
                root.add(datePicker, 1, 4);
                root.add(submitButton, 0, 5);
                root.add(backButton, 1, 5);
                root.add(successText, 0, 6, 2, 1);
                root.setHalignment(successText, HPos.CENTER);

                stage.setScene(new Scene(root, 500, 500));
                stage.show();

            }

            private void statLayout() {

                GridPane root = new GridPane();
                root.setAlignment(Pos.CENTER);
                root.setHgap(50);
                root.setVgap(15);


                Label avgRoomBookings = new Label("Avg. room bookings in a month: ");
                Label avgStudentBookings = new Label("Avg. student bookings in a month: ");
                Label mostFrequentlyUsed = new Label("Most frequently used room: ");
                Label mostActiveUser = new Label("Most active student: ");
                Label pickStudent = new Label("Student:");
                Label pickRoom = new Label("Room");

                Text mostFreqRoom = new Text();
                Text avgRoom = new Text();
                Text mostAct = new Text();
                Text avgStudBook = new Text();

                ObservableList<String> roomList = FXCollections.observableArrayList();
                ObservableList<String> studentListList = FXCollections.observableArrayList();
                for (int i = 0; i < FileController.getStudentsFromFile().size(); i++) {
                    studentListList.add(FileController.getStudentsFromFile().get(i).getName());
                }

                for (int i = 0; i < FileController.getRoomsFromFile().size(); i++) {
                    roomList.add(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()));
                }


                ComboBox studentBox = new ComboBox(studentListList);
                ComboBox roomBox = new ComboBox(roomList);


                backButton.setMinWidth(150);
                root.add(studentBox, 1, 1);
                root.add(roomBox, 1, 0);
                root.add(mostFreqRoom, 1, 5);
                root.add(avgStudBook, 1, 3);
                root.add(avgRoom, 1, 2);
                root.add(mostAct, 1, 4);
                root.add(avgRoomBookings, 0, 2);
                root.add(avgStudentBookings, 0, 3);
                root.add(mostActiveUser, 0, 4);
                root.add(mostFrequentlyUsed, 0, 5);
                root.add(backButton, 0, 6);
                root.add(pickRoom, 0, 0);
                root.add(pickStudent, 0, 1);
                backButton.setOnAction(event -> menuLayout());

                stage.setScene(new Scene(root, 500, 500));
                stage.show();
            }

            private void calendarLayout() {

                GridPane root = new GridPane();
                root.setAlignment(Pos.CENTER);
                root.setVgap(15);
                root.setHgap(15);

                ObservableList<String> roomNames = FXCollections.observableArrayList();
                DatePicker datePicker = new DatePicker(localDate);
                ComboBox roompicker = new ComboBox(roomNames);
                roompicker.setMinWidth(100);

                Button exitbutton = new Button("Back");
                exitbutton.setOnAction(event -> menuLayout());

                Button okbutton = new Button("OK");
                okbutton.setMinWidth(100);

                for (int i = 0; i < FileController.getStudentsFromFile().size(); i++) {
                    roomNames.add(String.valueOf(FileController.getRoomsFromFile().get(i).getNumber()));
                }

                ObservableList<Booking> data = FXCollections.observableArrayList();

                for (int i = 0; i < FileController.getBookingsFromFile().size(); i++) {
                    data.add(FileController.getStudentsFromFile().get(i).getName());
                }

                table.setEditable(true);

                TableColumn periodCol = new TableColumn("Period");
                TableColumn mondayCol = new TableColumn("Monday");
                TableColumn tuesdayCol = new TableColumn("Tuesday");
                TableColumn wednesdayCol = new TableColumn("Wednesday");
                TableColumn thursdayCol = new TableColumn("Thursday");
                TableColumn fridayCol = new TableColumn("Friday");
                TableColumn saturdayCol = new TableColumn("Saturday");
                TableColumn sundayCol = new TableColumn("Sunday");

                table.getColumns().addAll(periodCol, mondayCol, tuesdayCol, wednesdayCol,thursdayCol,fridayCol,saturdayCol,sundayCol);
              //  root.getChildren().addAll(datePicker, roompicker, table, exitbutton);
                root.add(datePicker, 0, 0);
                root.add(roompicker, 1, 0);
                root. add(okbutton, 2, 0);
                root.add(table, 0, 1, 3, 3);
                root.add(exitbutton, 1, 3);
                stage.setScene(new Scene(root, 650, 600));
                stage.show();


            }


            public static void main(String[] args) {
                launch(args);
            }
        }
