package sample;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Room{

    private int number;
    private boolean piano;

    public Room(int number, boolean piano) {
        this.number = number;
        this.piano = piano;
    }


    public static List getRooms(){
        List<Room> rooms = new ArrayList<>();
        Room r1 = new Room(101, true);
        Room r2 = new Room(102, false);
        Room r3 = new Room(103, false);
        Room r4 = new Room(104, false);
        Room r5 = new Room(105, false);
        Room r6 = new Room(106, false);

        rooms.add(r1);
        rooms.add(r2);
        rooms.add(r3);
        rooms.add(r4);
        rooms.add(r5);
        rooms.add(r6);

        saveInFile(rooms);

        return rooms;
    }

    public static void saveInFile (List room){
        try (ObjectOutputStream oos = new ObjectOutputStream
                (new FileOutputStream("rooms.ser"))) {
            oos.writeObject(room);
        } catch (IOException brutus) {
            brutus.printStackTrace();
        }
    }

    public static List readFromFile(){
        List rooms = new ArrayList(5);
        try (ObjectInputStream ois = new ObjectInputStream
                (new FileInputStream("rooms.ser"))) {
            rooms = (List) ois.readObject();
        } catch (InvalidClassException brutus) {
            brutus.printStackTrace();
        } catch (IOException | ClassNotFoundException brutus) {
            brutus.printStackTrace();
        }
        return rooms;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public boolean isPiano() {
        return piano;
    }

    public void setPiano(boolean piano) {
        this.piano = piano;
    }

    @Override
    public String toString() {
        String hasPiano = "";
        if (piano == true) {
            hasPiano = "with piano";
        } else{
            hasPiano = "without piano";
        }

        return "Practice room " +
                "number" + number + "; " +
                hasPiano;
    }
}
