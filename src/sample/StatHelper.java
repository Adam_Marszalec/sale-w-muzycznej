package sample;

import java.io.Serializable;
import java.util.List;

public class StatHelper {

    public String mostBookedRoom(List<Booking> bookings, List<Room> rooms) {
        Room room = null;
        int counter = 0;
        int mostBooked = 0;
        int[] sums = {0,0,0,0,0,0,0,0};

        for (int i = 1; i < rooms.size() + 1; i++) {
            for (int j = 0; j < bookings.size(); j++) {
                if (bookings.get(j).getRoom().getNumber() == i) {
                    counter++;
                }
            }
            sums[i - 1] = counter;
        }

        for (int i = 0; i < sums.length; i++) {
            if (mostBooked < sums[i]){
                mostBooked = i + 1;
            }
        }

        for (int i = 0; i < rooms.size(); i++) {
            if (mostBooked == rooms.get(i).getNumber()){
                room = rooms.get(i);
            }

        }
        return String.valueOf(room.getNumber());
    }





}
